
"""Conversation Manager module for handling conversation threads and messages.

This module allows storing, retrieving, and managing conversation threads within the OpenAI CLI application.
It uses JsonStorage for persisting conversation data across CLI sessions.
"""

import os
from typing import List, Dict
from ..api.json_storage import JsonStorage

# Default JSON file for storing conversations
DEFAULT_CONVERSATIONS_FILE = os.path.join('data', 'conversations.json')

# In-memory structure to store conversation threads if JSON storage is not used
CONVERSATIONS = {}

# Initialize JSON storage
storage = JsonStorage()

# Modify existing functions to integrate JSON storage
def add_message(thread_id: str, message: Dict):
    global CONVERSATIONS
    if thread_id not in CONVERSATIONS:
        CONVERSATIONS[thread_id] = []
    CONVERSATIONS[thread_id].append(message)
    # Save the updated thread to JSON storage
    storage.save(CONVERSATIONS, DEFAULT_CONVERSATIONS_FILE)

def get_thread(thread_id: str) -> List[Dict]:
    global CONVERSATIONS
    if not CONVERSATIONS:
        # Load conversations from JSON storage if empty
        CONVERSATIONS = storage.load(DEFAULT_CONVERSATIONS_FILE) or {}
    return CONVERSATIONS.get(thread_id, [])

def save_conversations():
    global CONVERSATIONS
    storage.save(CONVERSATIONS, DEFAULT_CONVERSATIONS_FILE)

def load_conversations():
    global CONVERSATIONS
    CONVERSATIONS = storage.load(DEFAULT_CONVERSATIONS_FILE) or {}

def list_all_threads() -> List[str]:
    global CONVERSATIONS
    if not CONVERSATIONS:
        load_conversations()  # Ensure we have loaded conversations before listing
    return list(CONVERSATIONS.keys())
