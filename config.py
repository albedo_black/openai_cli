"""Configuration settings for the OpenAI CLI application.
    
This module is responsible for setting up the necessary configuration variables
required to interact with the OpenAI API. It includes the retrieval of the API
key from environment variables and the definition of the base API URL.
"""

import os

# Define the base URL for the OpenAI API
API_BASE_URL = 'https://api.openai.com/v1'

# Function to retrieve the OpenAI API key from an environment variable.
def get_api_key():
    """Retrieve the OpenAI API key from the environment variables.
    
    Returns:
        str: The OpenAI API key.
    
    Raises:
        ValueError: If the OPENAI_API_KEY is not found in the environment variables.
    """
    api_key = os.getenv('OPENAI_API_KEY')
    if not api_key:
        error_message = (
            'The OpenAI API key is missing. Please set the OPENAI_API_KEY environment variable. '
            'For security reasons, it is best not to include the API key in the code.'
        )
        raise ValueError(error_message)
    return api_key
