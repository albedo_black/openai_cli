"""Authentication module for the OpenAI CLI application.

This module handles the retrieval of the OpenAI API key from environment variables,
ensuring that the application can authenticate against the OpenAI API.

"""

import os
from dotenv import load_dotenv

# Load the environment variables from a .env file if present
load_dotenv()

def get_api_key() -> str:
    """Retrieve the OpenAI API key from the environment variables.
    
    Returns:
        str: The OpenAI API key or None if not found.
        
    """
    return os.getenv('OPENAI_API_KEY')

# The calling code is responsible for handling the situation where the API key is not found.
