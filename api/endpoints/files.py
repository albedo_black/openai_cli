"""File endpoint module for OpenAI API interactions.

This module provides functions for managing files with the OpenAI API, including
listing, uploading, downloading, and deleting files.
"""

from ..request_handler import make_api_request
from openai_cli.utils.logging_util import setup_logging

# Configure logger for file endpoint operations
logger = setup_logging()

async def list_files():
    """List all files available in the OpenAI API account.

    Returns:
        list: A list of files from the API response.
    """
    logger.info("Listing all files.")
    try:
        return await make_api_request('files')
    except Exception as e:
        logger.error(f"Error listing files: {e}")
        raise

async def upload_file(file_path, purpose):
    """Upload a file to the OpenAI API.

    Args:
        file_path (str): Path to the file to be uploaded.
        purpose (str): The purpose of the file (e.g., 'answers', 'classifications').

    Returns:
        dict: The upload result from the API.
    """
    logger.info(f"Uploading file for purpose: {purpose}.")
    try:
        with open(file_path, 'rb') as file:
            files = {'file': file}
            data = {'purpose': purpose}
            return await make_api_request('files', 'POST', data=data, files=files)
    except Exception as e:
        logger.error(f"Error uploading file: {e}")
        raise

async def download_file(file_id):
    """Download a file from the OpenAI API.

    Args:
        file_id (str): The unique identifier for the file to be downloaded.

    Returns:
        bytes: The content of the file.
    """
    logger.info(f"Downloading file with ID: {file_id}.")
    try:
        return await make_api_request(f'files/{file_id}/content')
    except Exception as e:
        logger.error(f"Error downloading file: {e}")
        raise

async def delete_file(file_id):
    """Delete a file from the OpenAI API.

    Args:
        file_id (str): The unique identifier for the file to be deleted.

    Returns:
        dict: The deletion result from the API.
    """
    logger.info(f"Deleting file with ID: {file_id}.")
    try:
        return await make_api_request(f'files/{file_id}', 'DELETE')
    except Exception as e:
        logger.error(f"Error deleting file: {e}")
        raise
