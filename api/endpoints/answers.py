
"""Answers endpoint module for OpenAI API interactions.

This module provides the functionality to get answers to questions using the
OpenAI API's answers endpoint.
"""

from ..request_handler import make_api_request
from openai_cli.utils.logging_util import setup_logging

# Configure logger for the answers endpoint operations
logger = setup_logging()

async def create_answer(model: str, question: str, documents: list, examples_context: str, examples: list, max_tokens: int):
    """Create an answer using the OpenAI API.
    
    Args:
        model (str): The model to be used for generating answers.
        question (str): The question text to be answered.
        documents (list): A list of documents to search over.
        examples_context (str): The context used for examples.
        examples (list): A list of examples for training the ´answer´ model.
        max_tokens (int): The maximum number of tokens to generate in the completion.

    Returns:
        dict: The answer result from the API.
    """
    logger.info("Creating answer.")
    data = {
        "model": model,
        "question": question,
        "documents": documents,
        "examples_context": examples_context,
        "examples": examples,
        "max_tokens": max_tokens
    }
    try:
        return await make_api_request('answers', 'POST', json=data)
    except Exception as e:
        logger.error(f"Error creating answer: {e}")
        raise
