"""Completion endpoint module for OpenAI API interactions.

This module provides functionality to create completions using the OpenAI API
by sending requests to the completions endpoint with model and prompt information.
"""

from ..request_handler import make_api_request
from openai_cli.utils.logging_util import setup_logging

# Configure logger for completion endpoint operations
logger = setup_logging()

async def create_completion(model, prompt, **kwargs):
    """Creates a completion for the given model and prompt.
    
    Args:
        model (str): The model to use for the completion.
        prompt (str): The prompt to provide to the model.
        **kwargs: Additional parameters for the completion API call.
    
    Returns:
        dict: The completion result from the API.
    """
    logger.info(f"Creating completion with model: {model}, prompt: {prompt[:30]}...")
    data = {
        "model": model,
        "prompt": prompt,
        **kwargs
    }
    try:
        return await make_api_request('completions', 'POST', json=data)
    except Exception as e:
        logger.error(f"Error creating completion: {e}")
        raise
