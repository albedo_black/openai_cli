
"""Fine-tuning endpoint module for OpenAI API interactions.

This module provides the functionality to fine-tune models using the OpenAI API's fine-tuning endpoint.
"""

from ..request_handler import make_api_request
from openai_cli.utils.logging_util import setup_logging

# Configure logger for the fine-tune endpoint operations
logger = setup_logging()

async def start_fine_tuning(training_file: str, model: str, **kwargs):
    """Start a fine-tuning job for a given model with the specified training file.
    
    Args:
        training_file (str): The path to the file containing training data.
        model (str): The model to be fine-tuned.
        **kwargs: Additional parameters for fine-tuning.
    
    Returns:
        dict: The response from the API indicating the fine-tuning job details.
    """
    logger.info(f"Starting fine-tuning job for model: {model} with training file: {training_file}")
    data = {
        'training_file': training_file,
        'model': model,
        **kwargs
    }
    try:
        return await make_api_request('fine-tunes', 'POST', json=data)
    except Exception as e:
        logger.error(f"Error starting fine-tuning job: {e}")
        raise

async def list_fine_tuning_jobs():
    """List all fine-tuning jobs previously started.
    
    Returns:
        list: A list of fine-tuning jobs from the API response.
    """
    logger.info("Listing all fine-tuning jobs.")
    try:
        return await make_api_request('fine-tunes')
    except Exception as e:
        logger.error(f"Error listing fine-tuning jobs: {e}")
        raise

async def get_fine_tuning_job(job_id: str):
    """Get details for a specific fine-tuning job.
    
    Args:
        job_id (str): The unique identifier for the fine-tuning job.
    
    Returns:
        dict: The details for the specified fine-tuning job from the API.
    """
    logger.info(f"Retrieving details for fine-tuning job ID: {job_id}")
    try:
        return await make_api_request(f'fine-tunes/{job_id}')
    except Exception as e:
        logger.error(f"Error retrieving fine-tuning job details: {e}")
        raise
