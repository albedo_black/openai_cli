
"""Classifications endpoint module for OpenAI API interactions.

This module provides the functionality to perform text classification using the
OpenAI API's classification endpoint.
"""

from ..request_handler import make_api_request
from openai_cli.utils.logging_util import setup_logging

# Configure logger for the classifications endpoint operations
logger = setup_logging()

async def create_classification(model: str, query: str, examples: list, labels: list, search_model: str = None):
    """Create a classification using the OpenAI API.
    
    Args:
        model (str): The model to be used for classification.
        query (str): The query text to be classified.
        examples (list): A list of examples for training the classifier on-the-fly.
        labels (list): A list of possible labels for the classification.
        search_model (str, optional): An alternate model to use for semantic search when classifying.
    
    Returns:
        dict: The classification result from the API.
    """
    logger.info("Creating classification.")
    data = {
        "model": model,
        "query": query,
        "examples": examples,
        "labels": labels
    }
    if search_model:
        data["search_model"] = search_model
    try:
        return await make_api_request('classifications', 'POST', json=data)
    except Exception as e:
        logger.error(f"Error creating classification: {e}")
        raise
