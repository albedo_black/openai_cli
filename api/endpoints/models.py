"""Model endpoint module for OpenAI API interactions.

This module provides access to the available models and enables selecting a model
for various OpenAI API operations. It leverages caching to store model information
locally and only retrieves from the API upon request or when the cache is empty.
"""

from ..request_handler import make_api_request
from openai_cli.utils.logging_util import setup_logging

# Configure logger for the models endpoint operations
logger = setup_logging()

# In-memory cache for storing the list of models
MODELS_CACHE = None

async def get_available_models(force_refresh=False):
    """Retrieve the available OpenAI API models.

    Args:
        force_refresh (bool, optional): Force an API call to refresh the models. Defaults to False.

    Returns:
        list: A list of models from the API or the cache.
    """
    global MODELS_CACHE
    if MODELS_CACHE is None or force_refresh:
        try:
            logger.info("Retrieving model information from the OpenAI API.")
            models_data = await make_api_request('models')
            MODELS_CACHE = models_data.get('data', [])  # Update the cache
        except Exception as e:
            logger.error(f"Error retrieving models: {e}")
            raise
    else:
        logger.info("Retrieving model information from the cache.")
    return MODELS_CACHE

async def get_model_details(model_id):
    """Retrieve details for a specific OpenAI API model.

    Args:
        model_id (str): The unique identifier for the model.

    Returns:
        dict: The details of the specified model from the API.
    """
    try:
        logger.info(f"Retrieving details for model ID: {model_id}.")
        return await make_api_request(f'models/{model_id}')
    except Exception as e:
        logger.error(f"Error retrieving model details: {e}")
        raise
