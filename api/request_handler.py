"""Request handler module for the OpenAI CLI application.

This module contains functionality to make asynchronous HTTP requests to the
OpenAI API using the provided API key and specified endpoint details.
"""

import httpx
from openai_cli.api.auth import get_api_key  # Use updated auth module
from openai_cli.utils.logging_util import setup_logging

logger = setup_logging()

async def make_api_request(endpoint, method='GET', params=None, data=None, json=None):
    """Make a request to the OpenAI API.

    Args:
        endpoint (str): The API endpoint to call.
        method (str, optional): The HTTP method to use. Defaults to 'GET'.
        params (dict, optional): The query parameters for the request. Defaults to None.
        data (dict, optional): The form data for the request. Defaults to None.
        json (dict, optional): The JSON payload for the request. Defaults to None.

    Returns:
        dict: The parsed JSON response from the API.

    Raises:
        httpx.HTTPStatusError: An HTTP error occurred.
        httpx.RequestError: A request error occurred.
    """
    api_key = get_api_key()
    if not api_key:
        logger.error('API key not found. Please set the OPENAI_API_KEY environment variable.')
        return None  # Return None to signify that the request cannot be made without an API key

    headers = {
        'Authorization': f'Bearer {api_key}',
        'Content-Type': 'application/json'
    }

    url = f"https://api.openai.com/v1/{endpoint}"

    async with httpx.AsyncClient() as client:
        try:
            response = await client.request(method, url, headers=headers, params=params, data=data, json=json)
            response.raise_for_status()  # Raise an exception for HTTP error responses
            return response.json()
        except httpx.HTTPStatusError as e:
            logger.error(f'HTTP status error: {e.response.status_code}')
            raise
        except httpx.RequestError as e:
            logger.error(f'Request error: {e}')
            raise
