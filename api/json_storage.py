"""JSON Storage module for the OpenAI CLI application.

This module provides a JSON file-based storage strategy for persisting data across
CLI sessions, implementing the storage interface defined in the storage module.
"""

import json
from typing import Any
from .storage import StorageInterface

class JsonStorage(StorageInterface):
    """Concrete JSON file-based storage class."""

    def save(self, data: Any, location: str) -> None:
        """Save data to a JSON file at the specified location.
        
        Args:
            data (Any): The data to be saved, which must be JSON-serializable.
            location (str): The file path to the JSON file where data will be saved.
        """
        with open(location, 'w', encoding='utf-8') as file:
            json.dump(data, file, ensure_ascii=False, indent=4)

    def load(self, location: str) -> Any:
        """Load data from a JSON file at the specified location.
        
        Args:
            location (str): The file path to the JSON file from which to load data.
        
        Returns:
            Any: The data loaded from the JSON file, typically a dictionary or a list.
        """
        with open(location, 'r', encoding='utf-8') as file:
            return json.load(file)

# Example usage of the JsonStorage class
# storage = JsonStorage()
# storage.save({'key': 'value'}, 'path/to/data.json')
# data = storage.load('path/to/data.json')
