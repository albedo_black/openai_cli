"""Storage interface module for the OpenAI CLI application.

This module defines the interface and expected functionality for data storage strategies
employed within the application. It provides a foundation for implementing various
storage solutions, such as file-based or database storage.
"""

from abc import ABC, abstractmethod

class StorageInterface(ABC):
    """Abstract base class for storage interfaces."""
    
    @abstractmethod
    def save(self, data, location):
        """Save data to the specified storage location.
        
        Args:
            data: The data to be saved.
            location: The storage location (e.g., file path or database table).
        """
        pass

    @abstractmethod
    def load(self, location):
        """Load data from the specified storage location.
        
        Args:
            location: The storage location (e.g., file path or database table).
        
        Returns:
            The loaded data.
        """
        pass
