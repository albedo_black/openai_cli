"""Logging utility for the OpenAI CLI application.

This module configures the loggers used within the application, providing a
rotating file handler to manage log file sizes and retain logs across multiple
files up to a designated limit.
"""

import os
import logging
from logging.handlers import RotatingFileHandler

# Constants for default log file path and log level
DEFAULT_LOG_FILE = "openai_cli.log"
DEFAULT_LOG_LEVEL = logging.INFO

# Environment variables for log configuration
LOG_FILE_PATH = os.getenv('OPENAI_CLI_LOG_FILE', DEFAULT_LOG_FILE)
LOG_LEVEL = os.getenv('OPENAI_CLI_LOG_LEVEL', DEFAULT_LOG_LEVEL)

def setup_logging():
    """Sets up logging for the application with rotation.

    Returns:
        Logger: The configured logger instance for the application.
    """
    logger = logging.getLogger('openai_cli')
    logger.setLevel(LOG_LEVEL)

    # Define a rotating file handler to keep the logs manageable
    handler = RotatingFileHandler(
        LOG_FILE_PATH,
        maxBytes=5*1024*1024,  # 5 MB
        backupCount=5          # Keep up to 5 log files
    )
    
    # Log format includes timestamp, log level, and message
    formatter = logging.Formatter('[%(asctime)s] - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    
    logger.addHandler(handler)

    return logger
